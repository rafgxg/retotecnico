#!/bin/bash

cd nginx ; sudo docker build -t reto/node .
docker stop nginx-proxy
docker rm nginx-proxy
docker run -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock --link node-app:app  --name nginx-proxy reto/nginx-proxy
