#!/bin/bash

cd nodejs ; sudo docker build -t reto/node .
docker stop node-app
docker rm node-app
docker run -d -p 4000:4000 --name node-app reto/node
