#!/bin/bash

cd postgres ; sudo docker build -t reto/postgres .
docker stop postgres
docker rm postgres
docker run -d -p 5432 --name postgres reto/postgres
